<?php require_once(dirname(__FILE__) . '/../system/bootstrap.php');

use com\vogaro\infra\view\Template;
use com\vogaro\infra\dsl\form\FormDSL;
use com\vogaro\infra\validation\Validator;

use com\vogaro\context\form\entity\FormItemEntity;
use com\vogaro\context\form\entity\FormErr;
use com\vogaro\context\form\service\mail\FormAdminEmailService;
use com\vogaro\context\form\service\mail\FormUserEmailService;
use com\vogaro\context\form\repository\FormMasterRepository;

/**
 * 民泊フォームシステムの初期化
 */
return with(new FormDSL())
    /**
     * index,confirm,complete画面で$setting変数としてアクセスできるようになります。
     * フォーム全体の設定値などで使用します。
     */
    ->setting(function(){
        return array();
    })

    /**
     * $qはフォームから送信されたきた値になり、この値を使用してフォーム値の検証処理を行います。
     * 確認画面・完了画面に遷移する場合にここに処理が入ります。
     * 戻り値をfalseの判定になるようなものを返すと次の確認画面に進みます。
     */
    ->valid(function($q){

        $REQUIRED_TEXT      = '必須項目です。';
        $KATAKANA_TEXT      = '全角カタカナで入力してください。';
        $EMAIL_FORMAT_TEXT  = '正しくない%sです。';
        $TEL_FORMAT_TEXT    = '正しくない%sです。';

        /**============================
         * 基本登録情報の検証処理
         *============================*/
        $err = Validator::make()

            ->set(FormItemEntity::SEI, $q[FormItemEntity::SEI], '氏名(姓)')
            ->required($REQUIRED_TEXT)->end()

            ->set(FormItemEntity::MEI, $q[FormItemEntity::MEI], '氏名(名)')
            ->required($REQUIRED_TEXT)->end()

            ->set(FormItemEntity::SEI_KANA, $q[FormItemEntity::SEI_KANA], '氏名(セイ)')
            ->katakana($KATAKANA_TEXT)->required($REQUIRED_TEXT)->end()

            ->set(FormItemEntity::MEI_KANA, $q[FormItemEntity::MEI_KANA], '氏名(メイ)')
            ->katakana($KATAKANA_TEXT)->required($REQUIRED_TEXT)->end()

            ->set(FormItemEntity::MAIL, $q[FormItemEntity::MAIL], 'メールアドレス')
            ->email($EMAIL_FORMAT_TEXT)->required($REQUIRED_TEXT)->end()

            ->set(FormItemEntity::TEL1, $q[FormItemEntity::TEL1]. $q[FormItemEntity::TEL2]. $q[FormItemEntity::TEL3], '電話番号')
            ->tel($TEL_FORMAT_TEXT)->custom(function($v) use($q){
                return empty($q[FormItemEntity::TEL1]) ||
                    empty($q[FormItemEntity::TEL2]) ||
                    empty($q[FormItemEntity::TEL3]);
            }, $REQUIRED_TEXT)->end()

            ->set(FormItemEntity::INQUIRY_ITEM, $q[FormItemEntity::INQUIRY_ITEM], 'お問い合わせ項目')
            ->required($REQUIRED_TEXT)->end()


            //検証の設定項目を評価しエラー内容を返却
            ->valid()->getErr();

        return $err;
    })

    /**
     * トップページの処理を記述します。
     * フォーム項目の初期化・検証エラーの初期化・フロント側で使用したい設定情報を設定します。
     */
    ->index(function($query, $err, $setting){
        $formMasterRepository   = new FormMasterRepository();

        echo( Template::load(dirname(__FILE__) . '/tmpl/index.php', array(
            //エラーメッセージ
            'err'                           => new FormErr($err),
            // フォーム項目の設定値を定義
            'setting'                       => array(
                FormItemEntity::INQUIRY_ITEM          => $formMasterRepository->getInquiryItem(),
            ),
            //フォーム項目の初期化
            'item' => empty($query)?
                array(
                    FormItemEntity::SEI             => '',
                    FormItemEntity::MEI             => '',
                    FormItemEntity::SEI_KANA        => '',
                    FormItemEntity::MEI_KANA        => '',
                    FormItemEntity::COMPANY         => '',
                    FormItemEntity::MAIL            => '',
                    FormItemEntity::TEL1            => '',
                    FormItemEntity::TEL2            => '',
                    FormItemEntity::TEL3            => '',
                    FormItemEntity::INQUIRY_ITEM    => '',
                    FormItemEntity::BODY            => '',
                ): $query,
            /**
             * ヘルパー
             */
            'helper' => array(
                'checked'=> function($v1, $v2){
                    return $v1 == $v2? 'checked="checked"': '';
                },
                'err' => function($key1, $key2='') use($err){
                    $err1 = array_key_exists($key1, $err) ? $err[$key1] : '';
                    $err2 = array_key_exists($key2, $err) ? $err[$key2] : '';
                    return !empty($err1) ? $err1 : $err2;
                }
            )
        ))
        );
    })

    /**
     * 確認画面での処理を記述します。
     */
    ->confirm(function($query, $err, $setting){
        echo( Template::load(dirname(__FILE__) . '/tmpl/confirm.php', array('item' => $query)) );
    })

    /**
     * 完了画面での処理を記述します。
     * 管理者・ユーザにメールを送信します。
     */
    ->complete(function($item, $err, $setting){

        try{
            //管理者にメールを送信します。
            $from_admin = array('yoshimura@forus-and.co.jp' => 'お問い合わせ｜民泊5STAR');
            $title_admin = "【民泊5STAR】お問い合わせがありました。";
//            $to_admin = array('koyama@vogaro.co.jp' => 'お問い合わせ｜民泊5STAR');
			$to_admin = array(
				'yoshimura@forus-and.co.jp' => 'お問い合わせ｜民泊5STAR',
				'info@forus-and.co.jp'      => 'お問い合わせ｜民泊5STAR',
				'kawaguchi@forus-and.co.jp' => 'お問い合わせ｜民泊5STAR'
			);
            $formAdminEmailService = new FormAdminEmailService($item, $from_admin, $title_admin, $to_admin);
            $formAdminEmailService->send();

            //ユーザにメールを送信します。
            $from = array('yoshimura@forus-and.co.jp' => 'お問い合わせ｜民泊5STAR');
            $to_user = array($item[FormItemEntity::MAIL]);
            $title = "【民泊5STAR】お問い合わせしていただき、ありがとうございます。";
            $formUserEmailService = new FormUserEmailService($item, $from, $title, $to_user);
            $formUserEmailService->send();

        }catch(Exception $e){
            //TODO メール送信処理失敗
        }

        header('location: complete.php');
    })

    /**
     * 各メソッドて定義された内容を元にシステムを起動します。
     */
    ->run();
