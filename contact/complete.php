<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6 lt-ie7 lt-ie8 lt-ie9 lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 7]>
<html class="ie7 lt-ie8 lt-ie9 lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 8]>
<html class="ie8 lt-ie9  lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="ja" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>

	<?php require_once(dirname(__FILE__)."/../assets/inc/gtm-head.inc") ?>

	<meta charset="UTF-8">
	<title>お問い合わせ｜民泊物件投資の総合コンサルティングサービス - 5STAR</title>

	<meta name="keywords" content="民泊,物件,オーナー,コンサルティング,投資,利回り,リフォーム,リノベーション,運用,管理,問い合わせ">
	<meta name="description" content="「5STAR」は株式会社フォーラス＆カンパニーが運営する民泊総合コンサルティングサービスです。民泊物件を始めたい方、運用について相談したい方はもちろん、物件の査定や売買に関しても気軽にご相談ください。">

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta property="og:url" content="https://www.5star-forus.com/">
	<meta property="og:type" content="website">
	<meta property="og:title" content="物件オーナーのための民泊投資総合コンサルティングサービス - 5STAR">
	<meta property="og:image" content="https://www.5star-forus.com/assets/img/ogp.png">
	<meta property="og:description" content="「5STAR」は株式会社フォーラス＆カンパニーが運営する民泊投資総合コンサルティングサービスです。物件オーナー様のご要望をカタチにする物件リノベーション、集客、運用・管理により、民泊運用での資産を最大化いたします。">
	<meta property="og:site_name" content="物件オーナーのための民泊投資総合コンサルティングサービス - 5STAR">
	<meta property="og:locale" content="ja_JP">

	<?php require_once(dirname(__FILE__)."/../assets/inc/style.inc") ?>
	<link rel="stylesheet" href="../assets/css/contact/index.css">
</head>
<body>

	<?php require_once(dirname(__FILE__)."/../assets/inc/gtm-body.inc") ?>

	<script>
		dataLayer.push({
			'trackPageview': '/contact/complete.php',
			'event': 'loadready'
		});
	</script>

	<div class="l-page-wrapper">
		<div id="page" class="l-page">
			<div class="l-page__header">
				<?php $_GET['isForm'] = true; ?>
				<?php require_once(dirname(__FILE__)."/../assets/inc/header.php") ?>
				<?php unset($_GET['isForm']); ?>
			<!-- .l-page__header // --></div>

			<div class="l-page__main">

				<section class="sec-hd sec-hd--com">
					<div class="sec-hd__outline l-wrap">
						<h1 class="sec-hd__hdg">お問い合わせ</h1>
						<span class="line line--01"></span>
						<span class="line line--02"></span>
						<span class="line line--03"></span>
					</div><!-- .sec-hd__outline // -->
					<p class="sec-hd__lead">民泊の物件投資のことなら<br class="u-sp">お気軽にお問い合わせください。<br>物件の査定やリノベーション、<br class="u-sp">運用・管理のことはもちろん、<br class="u-sp">物件の売買についても<br class="u-sp">相談を承っております。</p>
					<p class="sec-hd__tel"><a href="tel:0668821777">お電話でのお問い合わせ 06-6882-1777 受付時間 10:00～18:00 年末年始は除く</a></p>
				</section><!-- .sec-hd // -->

				<section class="sec-form">
					<form name="form">
						<div class="sec-form__nav">
							<p class="item"><span>入力</span></p>
							<p class="item"><span>確認</span></p>
							<h1 class="item item--active"><span>完了</span></h1>
						</div><!-- .sec-form__arw // -->

						<div class="sec-form__outline">
							<div class="sec-form__base sec-form__base--complete">
								<p class="complete-hdg">お問い合わせありがとうございます</p>
								<p class="complete-lead">ご入力されたメールアドレス宛に確認のメールを送信いたしましたのでご確認ください。<br>数日経過してもメールが届かない場合には、ご入力時のメールアドレスが間違っている場合がありますので、誠に恐縮ですが再度のご連絡をよろしくお願いします。</p>
							</div><!-- .sec-form__base // -->

							<div class="sec-form__submit sec-form__submit">
								<a href="/" class="button button--style04 ico--im ico--arrow01"></a>
							</div>
						</div><!-- .sec-form__outline // -->
					</form>
				</section><!-- .sec-form // -->

			<!-- .l-page__main // --></div>

			<div class="l-page__footer">
				<?php $_GET['isForm'] = true; ?>
				<?php require_once(dirname(__FILE__)."/../assets/inc/footer.php") ?>
				<?php unset($_GET['isForm']); ?>
			<!-- .l-page__footer // --></div>

			<?php require_once(dirname(__FILE__)."/../assets/inc/global-nav.php") ?>
			<?php require_once(dirname(__FILE__)."/../assets/inc/indicator.inc") ?>

		<!-- .l-page // --></div>
	<!-- .l-page-wrapper // --></div>

	<!--#include virtual="/assets/inc/js.inc" -->
	<script src="../assets/js/app.js"></script>

</body>
</html>
