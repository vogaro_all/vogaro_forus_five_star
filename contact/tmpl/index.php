<?php
use com\vogaro\context\form\entity\FormItemEntity;

$checkedFn = $helper['checked'];
$errFn = $helper['err'];
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie6 lt-ie7 lt-ie8 lt-ie9 lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 7]>
<html class="ie7 lt-ie8 lt-ie9 lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 8]>
<html class="ie8 lt-ie9  lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie10" lang="ja" prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="ja" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>

	<?php require_once(dirname(__FILE__)."/../../assets/inc/gtm-head.inc") ?>

	<meta charset="UTF-8">
	<title>お問い合わせ｜民泊物件投資の総合コンサルティングサービス - 5STAR</title>

	<meta name="keywords" content="民泊,物件,オーナー,コンサルティング,投資,利回り,リフォーム,リノベーション,運用,管理,問い合わせ">
	<meta name="description" content="「5STAR」は株式会社フォーラス＆カンパニーが運営する民泊総合コンサルティングサービスです。民泊物件を始めたい方、運用について相談したい方はもちろん、物件の査定や売買に関しても気軽にご相談ください。">

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta property="og:url" content="https://www.5star-forus.com/contact/">
	<meta property="og:type" content="website">
	<meta property="og:title" content="お問い合わせ｜民泊物件投資の総合コンサルティングサービス - 5STAR">
	<meta property="og:image" content="https://www.5star-forus.com/assets/img/ogp.png">
	<meta property="og:description" content="5STAR」は株式会社フォーラス＆カンパニーが運営する民泊総合コンサルティングサービスです。民泊物件を始めたい方、運用について相談したい方はもちろん、物件の査定や売買に関しても気軽にご相談ください。">
	<meta property="og:site_name" content="物件オーナーのための民泊投資総合コンサルティングサービス - 5STAR">
	<meta property="og:locale" content="ja_JP">

	<?php require_once(dirname(__FILE__)."/../../assets/inc/style.inc") ?>

	<link rel="stylesheet" href="../assets/css/contact/index.css">
</head>
<body>

	<?php require_once(dirname(__FILE__)."/../../assets/inc/gtm-body.inc") ?>

	<script>
		dataLayer.push({
			'trackPageview': '/contact/input.html',
			'event': 'loadready'
		});
	</script>

	<div class="l-page-wrapper">
		<div id="page" class="l-page">
			<div class="l-page__header">
				<?php $_GET['isForm'] = true; ?>
				<?php require_once(dirname(__FILE__)."/../../assets/inc/header.php") ?>
				<?php unset($_GET['isForm']); ?>
			<!-- .l-page__header // --></div>

			<div class="l-page__main">

				<section class="sec-hd">
					<div class="sec-hd__outline l-wrap">
						<h1 class="sec-hd__hdg">お問い合わせ</h1>
						<span class="line line--01"></span>
						<span class="line line--02"></span>
						<span class="line line--03"></span>
					</div><!-- .sec-hd__outline // -->
					<p class="sec-hd__lead">民泊の物件投資のことなら<br class="u-sp">お気軽にお問い合わせください。<br>物件の査定やリノベーション、<br class="u-sp">運用・管理のことはもちろん、<br class="u-sp">物件の売買についても<br class="u-sp">相談を承っております。</p>
					<p class="sec-hd__tel"><a href="tel:0668821777">お電話でのお問い合わせ 06-6882-1777 受付時間 10:00～18:00 年末年始は除く</a></p>
				</section><!-- .sec-hd // -->

				<section class="sec-form">
					<form name="form" method="POST">
						<div class="sec-form__nav">
							<h1 class="item item--active"><span>入力</span></h1>
							<p class="item"><span>確認</span></p>
							<p class="item"><span>完了</span></p>
						</div><!-- .sec-form__arw // -->

						<div class="sec-form__outline">
							<div class="sec-form__base sec-form__base--input">

								<div class="input-block input-block--must input-block--style-name">
									<div class="col col--left">
										<p class="hdg">氏名</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<p class="hdg">姓</p>
											<p class="input"><input type="text" placeholder="山田" name="<?php echo(FormItemEntity::SEI); ?>" value="<?php echo($item[FormItemEntity::SEI]); ?>"></p>
										</div><!-- .box01 // -->
										<div class="box box--02">
											<p class="hdg">名</p>
											<p class="input"><input type="text" placeholder="太郎" name="<?php echo(FormItemEntity::MEI); ?>" value="<?php echo($item[FormItemEntity::MEI]); ?>"></p>
										</div><!-- .box02 // -->
										<p class="error"><?php echo($errFn(FormItemEntity::SEI, FormItemEntity::MEI)); ?></p>
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

								<div class="input-block input-block--must input-block--style-name">
									<div class="col col--left">
										<p class="hdg">氏名（フリガナ）</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<p class="hdg">セイ</p>
											<p class="input"><input type="text" placeholder="ヤマダ" name="<?php echo(FormItemEntity::SEI_KANA); ?>" value="<?php echo($item[FormItemEntity::SEI_KANA]); ?>"></p>
										</div><!-- .box01 // -->
										<div class="box box--02">
											<p class="hdg">メイ</p>
											<p class="input"><input type="text" placeholder="タロウ" name="<?php echo(FormItemEntity::MEI_KANA); ?>" value="<?php echo($item[FormItemEntity::MEI_KANA]); ?>"></p>
										</div><!-- .box02 // -->
										<p class="error"><?php echo($errFn(FormItemEntity::SEI_KANA, FormItemEntity::MEI_KANA)); ?></p>
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

								<div class="input-block input-block--style-flat">
									<div class="col col--left">
										<p class="hdg">会社名</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<p class="input"><input type="text" placeholder="例）株式会社5STAR" name="<?php echo(FormItemEntity::COMPANY); ?>" value="<?php echo($item[FormItemEntity::COMPANY]); ?>"></p>
										</div><!-- .box01 // -->
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

								<div class="input-block input-block--must input-block--style-flat">
									<div class="col col--left">
										<p class="hdg">メールアドレス</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<p class="input"><input type="email" placeholder="sample@5star-forus.com" name="<?php echo(FormItemEntity::MAIL); ?>" value="<?php echo($item[FormItemEntity::MAIL]); ?>"></p>
										</div><!-- .box01 // -->
																				<p class="error"><?php echo($errFn(FormItemEntity::MAIL)); ?></p>
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

								<div class="input-block input-block--must input-block--style-tel">
									<div class="col col--left">
										<p class="hdg">電話番号</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<p class="input"><input type="tel" placeholder="06" name="<?php echo(FormItemEntity::TEL1); ?>" value="<?php echo($item[FormItemEntity::TEL1]); ?>"></p>
										</div><!-- .box01 // -->
										<div class="box box--02">
											<p class="input"><input type="tel" placeholder="1234" name="<?php echo(FormItemEntity::TEL2); ?>" value="<?php echo($item[FormItemEntity::TEL2]); ?>"></p>
										</div><!-- .box02 // -->
										<div class="box box--03">
											<p class="input"><input type="tel" placeholder="5678" name="<?php echo(FormItemEntity::TEL3); ?>" value="<?php echo($item[FormItemEntity::TEL3]); ?>"></p>
										</div><!-- .box03 // -->
										<p class="error">
											<?php
												$tel_err = $errFn(FormItemEntity::TEL1);
												if ($tel_err) {
													echo($tel_err);
												} else {
													if ($telerr = $errFn(FormItemEntity::TEL2)) {
														echo($tel_err);
													}
													if ($telerr = $errFn(FormItemEntity::TEL3)) {
														echo($tel_err);
													}
												}
											?>
										</p>
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

								<div class="input-block input-block--must input-block--style-radio">
									<div class="col col--left">
										<p class="hdg">お問い合わせ項目</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<ul>
												<?php foreach($setting[FormItemEntity::INQUIRY_ITEM] as $index => $inquiry_item):?>
													<li>
														<input type="radio" id="radio01-0<?php echo($index);?>" name="<?php echo(FormItemEntity::INQUIRY_ITEM);?>" value="<?php echo($inquiry_item);?>" <?php echo($checkedFn($inquiry_item, $item[FormItemEntity::INQUIRY_ITEM])); ?>>
														<label for="radio01-0<?php echo($index);?>"><?php echo($inquiry_item); ?></label>
														<div class="check"></div>
													</li>
												<?php endforeach;?>
											</ul>
										</div><!-- .box01 // -->
										<p class="error"><?php echo($errFn(FormItemEntity::INQUIRY_ITEM)); ?></p>
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

								<div class="input-block input-block--style-textarea">
									<div class="col col--left">
										<p class="hdg">お問い合わせ内容</p>
									</div><!-- .col-left // -->
									<div class="col col--right">
										<div class="box box--01">
											<p class="input">
											<textarea name="<?php echo(FormItemEntity::BODY); ?>" ><?php echo($item[FormItemEntity::BODY]); ?></textarea></p>
										</div><!-- .box01 // -->
										<p class="error"><?php echo($errFn(FormItemEntity::BODY)); ?></p>
									</div><!-- .col-right // -->
								</div><!-- .input-block // -->

							</div><!-- .sec-form__base // -->

							<div class="sec-form__policy">
								<h2 class="hdg">ご利用規約</h2>
								<div class="block-lead">
									<h3>個人情報の取り扱いについて</h3>
									<h4 class="block-lead__ttl">1. 個人情報の利用目的</h4>
									<p class="block-lead__txt">お寄せいただいたお問い合わせに対する回答、連絡、管理のために利用いたします。</p>

									<h4 class="block-lead__ttl">2. 個人情報を第三者へ提供することについて</h4>
									<p class="block-lead__txt">当社はお預かりした個人情報を第三者へ提供する場合は、JIS Q 15001:2006が要求する必要事項を、本人通知し、同意いただかない限り実施いたしません。但し、以下の場合は同意を得ず提供することがございます。</p>
									<ul class="block-lead__list">
										<li>法令に基づく場合</li>
										<li>人の生命、身体又は財産保護のために緊急に必要がある場合であって、本人の同意を得ることが困難な場合</li>
										<li>公衆衛生上の向上又は児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難な場合</li>
										<li>国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要があって、本人の同意を得ることにより当該事務の遂行に支障を及ぼすおそれがある場合</li>
									</ul>

									<h4 class="block-lead__ttl">3. 個人情報の委託について</h4>
									<p class="block-lead__txt">当社は一部業務(給与計算、社会保険手続き業務)を会計事務所および社会保険労務士事務所へ委託して行っております。当社と会計事務所及び社会保険労務士事務所は『機密情報に関する覚書』を取り交わしたうえで業務を委託しております。</p>

									<h4 class="block-lead__ttl">4. 個人情報の開示等の問い合わせについて</h4>
									<p class="block-lead__txt">お預かりした個人情報の開示、利用目的の通知、訂正、追加又は削除、利用又は提供の拒否については、以下問い合わせ先までご一報ください。その際、ご登録いただいた情報をもとに本人確認をさせていただきます。</p>

									<h4 class="block-lead__ttl">5. 個人情報の任意性について</h4>
									<p class="block-lead__txt">必須項目をご入力いただけない場合は、お問い合わせができないことがございますのでご了承ください。</p>

									<h4 class="block-lead__ttl">6. お問い合わせ先</h4>
									<ul class="block-lead__list">
										<li>株式会社フォーラス&amp;カンパニー</li>
										<li>〒530-0041<br>大阪市北区天神橋2-2-11　阪急産業南森町ビル3階</li>
										<li>個人情報保護管理責任者　担当：吉村 貴成</li>
										<li>TEL：<a href="tel:0668821777">06-6882-1777</a></li>
										<li>FAX：06-6882-1770</li>
										<li>E-mail：<a href="mailto:yoshimura@forus-and.co.jp">yoshimura@forus-and.co.jp</a></li>
										<li>受付時間：10:00～18:00　年末年始は除く</li>
									</ul>
								</div>
								<p class="ico"></p>
							</div><!-- .sec-form__policy // -->

							<div class="sec-form__submit">
								<button type="submit" class="button button--style01 ico--im ico--arrow01" name="confirm"></button>
							</div>
						</div><!-- .sec-form__outline // -->
					</form>
				</section><!-- .sec-form // -->

			<!-- .l-page__main // --></div>

			<div class="l-page__Z">
				<?php $_GET['isForm'] = true; ?>
				<?php require_once(dirname(__FILE__)."/../../assets/inc/footer.php") ?>
				<?php unset($_GET['isForm']); ?>
			<!-- .l-page__footer // --></div>

			<?php require_once(dirname(__FILE__)."/../../assets/inc/global-nav.php") ?>
			<?php require_once(dirname(__FILE__)."/../../assets/inc/indicator.inc") ?>

		<!-- .l-page // --></div>
	<!-- .l-page-wrapper // --></div>

	<?php require_once(dirname(__FILE__)."/../../assets/inc/js.inc") ?>
	<script src="../assets/js/contact/app.js"></script>
</body>
</html>
