<div class="conversion js-area" data-label="conversion">
	<div class="conversion__inner l-wrap">
		<p class="conversion__lead">さぁ、5STARではじめよう</p>
		<div class="conversion__content">
			<div class="l-flex-grid">
				<div class="l-flex-grid__item l-flex-grid__item--1of2">
					<div class="feature">
						<p class="feature__lead">民泊への物件投資で<br class="u-sp">オーナー様の資産を最大化</p>
						<ul class="feature__list">
							<li>民泊での物件投資を始めたい。</li>
							<li>民泊をするために物件のリノベーションを考えている。</li>
							<li>所有している物件が民泊での運用に適しているかを査定してほしい。</li>
							<li>民泊物件を高く売りたい、物件オーナーになりたい。</li>
						</ul>
					<!-- .feature // --></div>
				<!-- .l-flex-grid__item // --></div>
				<div class="l-flex-grid__item l-flex-grid__item--1of2">
					<div class="contact">
						<p class="contact__btn"><a href="/contact/" class="c-btn c-btn--size01 c-btn--color01 c-btn--width01" target="_blank"><span class="ico ico--im ico--arrow01">お問い合わせ</span></a></p>
						<dl class="contact__detail detail">
							<dt class="detail__hdg">お電話でのお問い合わせ</dt>
							<dd class="detail__number"><a href="tel:0668821777">06-6882-1777</a></dd>
							<dd class="detail__info">受付時間 10:00～18:00 年末年始は除く <br class="u-sp">携帯電話・PHSからもご利用頂けます。</dd>
						</dl>
					</div>
				<!-- .l-flex-grid__item // --></div>
			<!-- .l-flex-grid // --></div>
		<!-- .conversion__content // --></div>
	<!-- .l-wrap // --></div>
<!-- .conversion // --></div>
