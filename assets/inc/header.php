<header id="global-header" class="global-header">
	<?php if (isset($_GET['level']) && $_GET['level'] == 'top') { ?>
		<h1 class="global-header__logo js-header-scroll-toggle"><a href="/" class="link">5 STAR</a></h1>
	<?php } else if (isset($_GET['isForm']) && $_GET['isForm'] == true) { ?>
		<p class="global-header__logo js-header-scroll-toggle"><span class="link">5 STAR</span></p>
	<?php } else if (isset($_GET['isNewsDetail']) && $_GET['isNewsDetail'] == true) { ?>
		<p class="global-header__logo js-header-scroll-toggle"><a href="/" class="link">5 STAR</a></p>
		<p class="global-header__back-to-news-list">
			<a href="/news/" class="c-btm c-btn--size02 c-btn--color04 c-btn--width02">
				<span class="ico ico--im ico--arrow05">
					<img src="/assets/img/txt_back_to_news_list.png" width="78" height="15" alt="一覧に戻る">
				</span>
			</a>
		</p>
	<?php } else { ?>
		<p class="global-header__logo js-header-scroll-toggle"><a href="/" class="link">5 STAR</a></p>
		<p class="global-header__back-to-top">
			<a href="/" class="c-btm c-btn--size02 c-btn--color04 c-btn--width02">
				<span class="ico ico--im ico--arrow05">
					<img src="/assets/img/txt_back_to_top.png" width="84" height="15" alt="TOPに戻る">
				</span>
			</a>
		</p>
	<?php } ?>

	<p class="global-header__contact">
		<a href="/contact/" class="c-btn c-btn--size02 c-btn--color01 c-btn--width02" target="_blank">
			<span class="ico ico--im ico--arrow03">お問い合わせ</span>
		</a>
	</p>

	<div class="global-header__menu-btn js-header-scroll-toggle">
		<a id="nav-trigger" class="menu-btn">
			<span class="line-default">
				<span class="line-default__item line-default__item--01"></span>
				<span class="line-default__item line-default__item--02"></span>
				<span class="line-default__item line-default__item--03"></span>
			</span>
			<span class="line-close">
				<span class="line-close__item line-close__item--01"></span>
				<span class="line-close__item line-close__item--02"></span>
			</span>
		<!-- .menu-btn // --></a>
	<!-- .global-header__menu-btn // --></div>
<!-- .global-header // --></header>
