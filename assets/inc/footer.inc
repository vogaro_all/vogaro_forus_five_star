<footer class="global-footer">
	<p class="global-footer__logo"><a href="http://www.forus-and.co.jp/" target="_blank">PRODUCED BY FORUS &amp; COMPANY</a></p>
	<ul class="global-footer__links">
		<li class="link-item"><a href="/news/">お知らせ</a></li>
		<li class="link-item"><a href="http://www.forus-and.co.jp/privacy/" target="_blank">個人情報保護方針</a></li>
	</ul>
	<p class="global-footer__copyright"><small>COPYRIGHT &copy; FORUS &amp; COMPANY ,  ALL RIGHTS RESERVED.</small></p>
<!-- .global-footer // --></footer>
