<div id="indicator" class="global-indicator">
	<ul class="indicator">
		<li class="indicator__item"><a href="#page" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>T</span><span>O</span><span>P</span></span></li>
		<li class="indicator__item"><a href="#sec-potential" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>民</span><span>泊</span><span>の</span><span>可</span><span>能</span><span>性</span></span></li>
		<li class="indicator__item"><a href="#sec-start" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>F</span><span>I</span><span>V</span><span>E</span><span>&nbsp;</span><span>S</span><span>T</span><span>A</span><span>R</span><span>に</span><span>つ</span><span>い</span><span>て</span></span></li>
		<li class="indicator__item"><a href="#sec-consulting" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>総</span><span>合</span><span>コ</span><span>ン</span><span>サ</span><span>ル</span><span>テ</span><span>ィ</span><span>ン</span><span>グ</span></span></li>
		<li class="indicator__item"><a href="#sec-creative" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>ク</span><span>リ</span><span>エ</span><span>イ</span><span>テ</span><span>ィ</span><span>ブ</span></span></li>
		<li class="indicator__item"><a href="#sec-management" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>マ</span><span>ネ</span><span>ジ</span><span>メ</span><span>ン</span><span>ト</span></span></li>
		<li class="indicator__item"><a href="#sec-case" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>施</span><span>工</span><span>事</span><span>例</span><span>と</span><span>実</span><span>績</span></span></li>
		<li class="indicator__item"><a href="#sec-flow" class="dot js-indicator-dot js-scroller"><span></span></a><span class="label"><span>ご</span><span>利</span><span>用</span><span>の</span><span>流</span><span>れ</span></span></li>
	</ul>
</div>
